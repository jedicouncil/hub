---
title: Privacy
description: Find here the privacy of R2Devops
---

## We respect you
R2Devops does not collect your personal data! We do track the visits on our website, but by using [Plausible.io](https://plausible.io/data-policy){:target="_blank"}, a GDPR compliant solution, we ensure no personal data are stored. With Plausible, we don't use cookies and respect the privacy of our website visitors.

## Privacy Policy
R2Devops’s Privacy Policy details the different ways personal data received from users are collected via the Website.

!!! success "It's simple, we do not store any personal data except your GitLab username if you are a contributor."


--8<-- "includes/abbreviations.md"
